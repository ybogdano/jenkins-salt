include:
    - slave.dhcp
    - slave.packages
    - slave.ssh_keys
    - slave.sysctl
    - slave.systemd
    - slave.jenkins_online_check
    - slave.kernel
