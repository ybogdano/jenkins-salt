include:
    - test.minion
# remove rolling mesa/libc updates to improve stability
#    - test.packages
{% if salt.pkg.version_cmp(salt.pkg.version('openssh-server'), '1:8.3p1-1') != -1 %}
    - test.sshd
{% endif %}
