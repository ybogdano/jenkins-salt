/etc/ssh/sshd_config:
    file.managed:
        - source: salt://test/files/sshd_config
        - user: root
        - group: root
        - mode: 644

sshd:
    service.running:
        - reload: True
        - watch:
            - file: /etc/ssh/sshd_config
