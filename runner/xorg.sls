include:
  - slave.packages

/etc/X11/Xwrapper.config:
    file.managed:
        - source: salt://runner/files/xorg/Xwrapper.config
        - user: root
        - group: root
        - mode: 644

xserver-packages:
    pkg.installed:
      - require:
        - sls: slave.packages
      - refresh: True
      - normalize: False
      - pkgs:
          - xserver-xorg-legacy
          - xserver-xorg-core
          - x11-xserver-utils

/etc/X11/xorg.conf.d/30-dmabuf.conf:
    file.managed:
        - source: salt://runner/files/30-dmabuf.conf
        - user: root
        - group: root
        - mode: 644
        - makedirs: True
