/etc/default/grub:
  file.managed:
    - source: salt://runner/files/grub
    - user: root
    - group: root
    - mode: 644

'update-grub -o /boot/efi/EFI/debian/grub.cfg':
    cmd.run:
        - onchanges:
            - file: /etc/default/grub

# vim: ft=yaml
