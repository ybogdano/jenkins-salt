include:
    - base.timesyncd
    - slave.apt

perf-packages:
    pkg.installed:
        - require:
            - sls: base.timesyncd
            - sls: slave.apt
        - refresh: True
        - normalize: False
        - pkgs:
          - x11-xserver-utils
          - openbox
          - xinit
          - python3-pandas
          - docker
          - docker-compose
          # required for python3-opencv
          - libopencv-calib3d4.2
          - libopencv-features2d4.2
          - libopencv-highgui4.2
          - libopencv-imgcodecs4.2
          - libopencv-objdetect4.2
          - libopencv-shape4.2
          - libopencv-stitching4.2
          - libopencv-superres4.2
          - libopencv-video4.2
          - libopencv-videoio4.2
          - libopencv-videostab4.2
          - libopencv-contrib4.2
          - libhdf5-103-1
          - libgdal26
          - libopencv-viz4.2
          - libvtk6.3
          - libogdi4.1
          # not needed for testing, but useful for CI management tasks/debug
          - x11vnc
