/etc/sudoers.d/20-jenkins-fix-freq:
  file.managed:
    - source: salt://perf/files/20-jenkins-fix-freq
    - user: root
    - group: root
    - mode: 600
    - require:
        - file: /etc/sudoers.d

# vim: ft=yaml
