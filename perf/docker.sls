docker:
  pkg.installed:
    - name: docker
  service.running:
    - enable: True
    - require:
        - pkg: docker
  group.present:
    - addusers:
        - jenkins
    - require:
        - pkg: docker

/etc/docker/daemon.json:
  file.managed:
    - source: salt://perf/files/docker-daemon.json
    - user: root
    - group: root
    - mode: 600
    - require:
        - pkg: docker
